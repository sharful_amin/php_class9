<?php

include('vendor/autoload.php');

use Calc\Calculator;
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Calculator</title>
  </head>
  <body>
        <div class="container-fluid bg-success">

        <div class="row d-flex justify-content-center align-items-center" style="height:600px;">

                <div class="col-6 bg-white" style="height:400px;"> 
                        <form action="" method="">
                                <fieldset>
                                            <legend>Calculator:</legend>
                                            <div class="row mt-4">
                                                <div class="col-12">
                                                    <label for="num1" class="float-start">NUMBER1 :</label>
                                                    <input type="number" id="num1" name="num1" class="w-75 float-end" placeholder="enter the first number" required>
                                                </div>
                                            </div>

                                            <div class="row mt-4">
                                                <div class="col-12">
                                                    <label for="num2" class="float-start">NUMBER2 :</label>
                                                    <input type="number" id="num2" name="num2" class="w-75 float-end ml-5" placeholder="enter the second number" required>
                                                </div>
                                            </div>

                                            <div class="row mt-4">
                                                <div class="col-12">
                                                    <label for="operation" class="float-start">Select the Operation :</label>
                                                    <select name="operation" id="operation" class="w-75 float-end py-1">
                                                        <option value="select one">Select One :</option>
                                                        <option value="plus">+</option>
                                                        <option value="minus">-</option>
                                                        <option value="multiplication">*</option>
                                                        <option value="division">/</option>
                                                        <option value="modulus">%</option>
                                                    </select>
                                                </div>
                                            </div>

                                </fieldset>
                        </form>
                </div>

        </div>

        </div>











        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>